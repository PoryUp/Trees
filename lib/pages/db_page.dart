//plik przygotowany na umiejscowienie tabeli z wynikami z bazy danych
import 'package:flutter/material.dart';

class DBase extends StatefulWidget {
  const DBase({Key? key}) : super(key: key);

  @override
  _DBaseState createState() => _DBaseState();
}

class _DBaseState extends State<DBase> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[50],
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 10),
              Flexible(
                child: Text(
                  'Miejsce na wyszukiwanie',
                  style: TextStyle(
                    fontSize: 25.0,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
