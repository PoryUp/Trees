import 'package:flutter/material.dart';
import 'package:new_tree/const/input.dart';
import 'package:new_tree/const/json.dart';
import 'package:new_tree/pages/log_in.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final TextEditingController _name = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _confirmpassword = TextEditingController();
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  //dla obu haseł
  bool _isObscure = true;
  bool _isObscure_confirm = true;

  @override
  Widget build(BuildContext context) {
    final isKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;
    return Scaffold(
      backgroundColor: Colors.green[50],
      body: Center(
        child: SingleChildScrollView(
          child: Form(
            key: _formkey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (!isKeyboard) buildLogo(),
                const SizedBox(
                  height: 15,
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                  child: TextFormField(
                      controller: _name,
                      keyboardType: TextInputType.text,
                      decoration: buildInputDecoration(Icons.person, 'Login'),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'To pole musi być uzupełnione';
                        }
                        if (value.length < 6 || value.length > 20) {
                          return 'To pole musi miec od 6-20 znaków';
                        }
                        return null;
                      }),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                  child: TextFormField(
                      controller: _email,
                      keyboardType: TextInputType.text,
                      decoration: buildInputDecoration(Icons.email, 'E-mail'),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'To pole jest wymagane';
                        }

                        // walidacja emaila
                        if (!RegExp(
                                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                            .hasMatch(value)) {
                          return "Wpisz poprawny Email";
                        }
                        return null;
                      }),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                  child: TextFormField(
                    obscureText: _isObscure,
                    controller: _password,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.lock),
                      hintText: "Hasło",
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        borderSide: BorderSide(color: Colors.green, width: 1.5),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        borderSide: BorderSide(
                          color: Colors.black,
                          width: 1.5,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        borderSide: BorderSide(
                          color: Colors.black,
                          width: 1.5,
                        ),
                      ),
                      suffixIcon: IconButton(
                        icon: Icon(
                            _isObscure
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.green.shade200),
                        onPressed: () {
                          setState(
                            () {
                              _isObscure = !_isObscure;
                            },
                          );
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'To pole musi być uzupelnione';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                  child: TextFormField(
                    obscureText: _isObscure_confirm,
                    controller: _confirmpassword,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.lock),
                      hintText: "Hasło",
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        borderSide: BorderSide(color: Colors.green, width: 1.5),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        borderSide: BorderSide(
                          color: Colors.black,
                          width: 1.5,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        borderSide: BorderSide(
                          color: Colors.black,
                          width: 1.5,
                        ),
                      ),
                      suffixIcon: IconButton(
                        icon: Icon(
                            _isObscure_confirm
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.green.shade200),
                        onPressed: () {
                          setState(
                            () {
                              _isObscure_confirm = !_isObscure_confirm;
                            },
                          );
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'To pole musi być uzupelnione';
                      }
                      if (value != _password.text) {
                        return 'Hasła się nie zgadzają';
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        LogIn()));
                          },
                          child: Text("Masz konto?")),
                      RaisedButton(
                        color: Colors.redAccent,
                        onPressed: () {
                          if (_formkey.currentState!.validate()) {
                            RegistrationUser();
                          }
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.black, width: 2)),
                        textColor: Colors.white,
                        child: Text("Zarejestruj się"),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future RegistrationUser() async {
    // url do rejestracji
    var APIURL =
        Uri.parse("https://polemical-attachmen.000webhostapp.com/register.php");
    //json map dla uzytkownika
    Map mapeddate = {
      'name': _name.text,
      'email': _email.text,
      'password': _password.text
    };
    //dane do php
    http.Response response = await http.post(APIURL, body: mapeddate);
    //z php z powrotem
    // print(response.body);
    // print(json.decode(response.body));

    RegisterResponse registerResponce =
        RegisterResponse.fromJson(json.decode(response.body));
    //po odczytaniu jsona sprawdzenie ktora akcja sie udała
    if (registerResponce.status == 1) {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => LogIn()));
    } else if (registerResponce.status == 3) {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => Register()));
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Użytkownik lub Email istnieją')));
    } else {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => Register()));
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Rejestracja nie powiodła się')));
    }
  }
}
