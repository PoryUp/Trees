import 'dart:convert';

import 'package:flutter/material.dart'; //głowny
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart'; //od image_picker
import 'package:new_tree/const/geo.dart';
import 'package:new_tree/const/input.dart'; //zwrot const dla inputów
import 'package:http/http.dart' as http; //http paczka do wysyłania
import 'dart:io'; //in out
import 'package:new_tree/my_icons_icons.dart'; //dla niestandardowych ikon
import 'package:new_tree/pages/log_in.dart'; //by móc sie wylogowac

class Formularz extends StatefulWidget {
  final String? id_user;
  const Formularz(this.id_user);

  @override
  _FormularzState createState() => _FormularzState();
}

class _FormularzState extends State<Formularz> {
  //kontrolery i wywołane argumenty
  TextEditingController _name_pol = TextEditingController();
  TextEditingController _name_lac = TextEditingController();
  double? _long;
  double? _lat;
  TextEditingController _description = TextEditingController();
  bool? _checkbox = false;
  TextEditingController _opis = TextEditingController();
  // TextEditingController _name = TextEditingController();
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  File? _imageFile;
  var img;
//funkcja wywołująca kamerę i osadzająca zdjecie

  void _getFromCamera() async {
    XFile? PickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxHeight: 1080,
      maxWidth: 1080,
    );
    setState(() {
      _imageFile = File(PickedFile!.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 50,
        actions: [
          TextButton(
              onPressed: () {
                //wylogowanie z aplikacji przycisk
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (BuildContext context) => LogIn(),
                  ),
                );
                ScaffoldMessenger.of(context)
                    .showSnackBar(const SnackBar(content: Text('Wylogowano')));
              },
              child: Text('Wyloguj się'),
              style: TextButton.styleFrom(primary: Colors.white))
        ],
        backgroundColor: Colors.green[300],
      ),
      backgroundColor: Colors.green[50],
      body: ListView(
        children: [
          Center(
            child: Form(
              key: _formkey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: TextFormField(
                      controller: _name_pol,
                      keyboardType: TextInputType.text,
                      decoration: buildInputDecoration(
                          MyIcons.forest, "Nazwa drzewa polska"),
                      validator: (value) {
                        if (value == null || value.isEmpty) {}
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: TextFormField(
                      controller: _name_lac,
                      keyboardType: TextInputType.text,
                      decoration: buildInputDecoration(
                          MyIcons.forest, "Nazwa drzewa łacińska"),
                      validator: (value) {
                        if (value == null || value.isEmpty) {}
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: TextFormField(
                      controller: _opis,
                      keyboardType: TextInputType.text,
                      decoration: buildInputDecoration(MyIcons.forest, "Opis"),
                      validator: (value) {
                        if (value == null || value.isEmpty) {}
                        return null;
                      },
                    ),
                  ),
                  CheckboxListTile(
                    //checkbox odznaczający
                    title: Text("Pomnik Przyrody"),
                    value: _checkbox,
                    onChanged: (newValue) {
                      setState(() {
                        _checkbox = newValue;
                      });
                    },
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        icon: Icon(Icons.photo_camera_sharp),
                        color: Colors.green.shade500,
                        onPressed: () {
                          _getFromCamera();
                        },
                      ),
                      IconButton(
                        icon: Icon(Icons.location_on),
                        color: Colors.green.shade500,
                        onPressed: () async {
                          Position position = await getGeoLocationPosition();
                          _long = position.longitude;
                          _lat = position.latitude;
                        },
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.redAccent,
                          onPrimary: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50.0),
                              side: BorderSide(
                                  color: Colors.green.shade900, width: 2)),
                        ),
                        onPressed: () {
                          if (_formkey.currentState!.validate()) {
                            img = _imageFile?.path.isEmpty;

                            if (img == null) {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          Formularz(widget.id_user)));
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content:
                                          Text('Zdjęcie jest obowiązkowe')));
                            } else {
                              Form_upload(widget.id_user);
                            }

                            //sprawdzanie
                            //widget.name wyciaga z wywołania klasy wartosci
                          }
                        },
                        child: Text("Wyślij"),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future Form_upload(id_user) async {
    String idUs = id_user;
    var s_check = _checkbox.toString();
    var lat = _lat.toString();
    var long = _long.toString();

    if (_name_lac.text.isEmpty) {
      _name_lac.text = 'null';
    }
    if (_name_pol.text.isEmpty) {
      _name_pol.text = 'null';
    }
    print(_imageFile);
    print(_lat);
    print(_long);
    // url do polaczenia z php
    var APIURL =
        Uri.parse("https://polemical-attachmen.000webhostapp.com/upload.php");

    String base64Image = base64Encode(_imageFile!.readAsBytesSync());
    String fileName = _imageFile!.path.split("/").last;

    //rozpoczecie multipartrequest umozliwiajace przesyłanie zdjec i innych danych
    var response = http.MultipartRequest('POST', APIURL);
    var image = await http.MultipartFile.fromString("image", base64Image);
    response.fields['id_user'] = id_user;
    response.fields['pl_name'] = _name_pol.text;
    response.files.add(image);
    response.fields['name'] = fileName;
    response.fields['lac_name'] = _name_lac.text;
    response.fields['longitude'] = long;
    response.fields['latitude'] = lat;
    response.fields['is_pomnik'] = s_check;
    response.fields['description'] = _opis.text;
    //wywołanie przesyłu
    var res = await response.send();

    //akcja drogi
    if (res.statusCode == 200) {
      print(res.statusCode);
      // resetuje stronę z tym samym kodem co mamy
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => Formularz(idUs)));
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Formularz wysłany powodzeniem')));
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Wysyłanie nieudane')));
    }
  }
}
