import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:new_tree/const/input.dart';
import 'package:new_tree/const/json.dart';
import 'package:new_tree/pages/form_page.dart';
import 'package:new_tree/pages/register.dart';

class LogIn extends StatefulWidget {
  const LogIn({Key? key}) : super(key: key);

  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  TextEditingController _name = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmpassword = TextEditingController();
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  //do zasloniecia hasła
  bool _isObscure = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[50],
      body: Center(
        child: Form(
          key: _formkey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              buildLogo(),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                child: TextFormField(
                  controller: _name,
                  keyboardType: TextInputType.text,
                  decoration: buildInputDecoration(Icons.person, "Login"),
                  validator: (value) {
                    if (value == null ||
                        value.isEmpty ||
                        value.length < 6 ||
                        value.length > 20) {
                      return 'To pole musi mieć wiecej niż 6 i mniej niż 20 znaków';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                child: TextFormField(
                  obscureText: _isObscure,
                  controller: _password,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.lock),
                    hintText: "Hasło",
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(color: Colors.green, width: 1.5),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                    ),
                    suffixIcon: IconButton(
                      icon: Icon(
                          _isObscure ? Icons.visibility : Icons.visibility_off,
                          color: Colors.green.shade200),
                      onPressed: () {
                        setState(
                          //umozliwiajace podglądniecie hasła
                          () {
                            _isObscure = !_isObscure;
                          },
                        );
                      },
                    ),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'To pole musi być uzupelnione';
                    }
                    return null;
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                      onPressed: () {
                        //przejscie do rejestracji
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (BuildContext context) => Register()));
                      },
                      child: Text("Nie masz konta?")),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.redAccent,
                      onPrimary: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50.0),
                          side: BorderSide(
                              color: Colors.green.shade900, width: 2)),
                    ),
                    onPressed: () {
                      if (_formkey.currentState!.validate()) {
                        LoginUser(); //wywolanie logowania
                      }
                    },
                    child: Text("Zaloguj"),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future LoginUser() async {
    var APIURL =
        Uri.parse("https://polemical-attachmen.000webhostapp.com/login.php");
    //Mapowanie jsona
    Map mapeddate = {'name': _name.text, 'password': _password.text};
    //wysłanie do php danych
    http.Response response = await http.post(APIURL, body: mapeddate);
    //zwrocenie i odszyfrowanie danych zwrotnych od JSONa
    LoginResponse loginResponce =
        LoginResponse.fromJson(json.decode(response.body));

    var id_user = loginResponce.userArr?.id;
    // var data = reponse.body;
    if (loginResponce.status == 1) {
      //wraz z logowaniem automatycznie do formularzu
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => Formularz(id_user)));
      ;
    } else {
      Navigator.of(context).pushReplacement(// zresetowanie panelu loginu
          MaterialPageRoute(builder: (BuildContext context) => LogIn()));
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Logowanie nie udane')));
    }
  }
}
