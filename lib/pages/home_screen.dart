//aktualnie nie uzywane dla uzytkownika. Rozbudowa w natepnym etapie nie na czas
import 'package:flutter/material.dart';
import 'package:new_tree/pages/DB_page.dart';
import 'package:new_tree/pages/form_page.dart';
import 'package:new_tree/pages/log_in.dart';
import 'package:new_tree/pages/main_page.dart';
import 'package:new_tree/pages/map_page.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    //menu na dolnym pasku,
    Menu(),
    Mapa(),
    DBase(),
    Formularz(""),
  ];
//funkcja umozliwiajaca zmiane wywoływanego pliku
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          TextButton(
              onPressed: () {
                //wyloguj sie
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (BuildContext context) => LogIn()));
                ScaffoldMessenger.of(context)
                    .showSnackBar(const SnackBar(content: Text('Wylogowano')));
              },
              child: Text('Wyloguj się'),
              style: TextButton.styleFrom(primary: Colors.white))
        ],
        backgroundColor: Colors.green[300],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.asset('assets/logo.png', fit: BoxFit.contain, height: 50),
            SizedBox(width: 40),
            Text(
              'Polska Baza drzew',
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w900,
                  fontFamily: 'arial black'),
            ),
          ],
        ),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Strona główna',
            backgroundColor: Colors.green[300],
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.public),
            label: 'Mapa',
            backgroundColor: Colors.green,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.sd_card),
            label: 'Lista drzew',
            backgroundColor: Colors.green[400],
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_circle),
            label: 'Dodaj obiekt',
            backgroundColor: Colors.greenAccent[200],
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue[900],
        onTap: _onItemTapped,
      ),
    );
  }
}
