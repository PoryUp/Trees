import 'package:flutter/material.dart';
import 'package:new_tree/pages/log_in.dart';

void main() => runApp(
      const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: LogIn(),
      ),
    );
//kod odpowiedzialny za wyustartowanie wszystkiego