import 'package:flutter/material.dart';

//Jest to klasa zajmujący się stylizacją textfieldów które nie są związane z hasłami
InputDecoration buildInputDecoration(IconData icons, String hinttext) {
  return InputDecoration(
    hintText: hinttext,
    prefixIcon: Icon(icons),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(25.0),
      borderSide: BorderSide(color: Colors.green, width: 1.5),
    ),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(25.0),
      borderSide: BorderSide(
        color: Colors.blue,
        width: 1.5,
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(25.0),
      borderSide: BorderSide(
        color: Colors.black,
        width: 1.5,
      ),
    ),
  );
}

//zewnetrzny z logiem
CircleAvatar buildLogo() {
  return CircleAvatar(
    backgroundColor: Colors.green[800],
    radius: 70,
    child: Image.asset(
      'assets/logo.png',
    ),
  );
}
