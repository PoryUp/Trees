//plik z klasami związanymi z inforamcjami zwracanymi przez JSON

//klasa zwracająca wyniki zwrotne przy logowaniu, w celu poinformowania uzytkownika co się stało.
class LoginResponse {
  int? status;
  String? statusText;
  UserArr? userArr;

  LoginResponse({this.status, this.statusText, this.userArr});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    statusText = json['status_text'];
    userArr = json['user_arr'] != null
        ? new UserArr.fromJson(json['user_arr'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['status_text'] = this.statusText;
    if (this.userArr != null) {
      data['user_arr'] = this.userArr!.toJson();
    }
    return data;
  }
}

//nie uzywane bezpośrednio ale uzywane zpw wywołaniu przez powyższy
class UserArr {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? password;
  Null? login;

  UserArr(
      {this.id, this.name, this.email, this.phone, this.password, this.login});

  UserArr.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    password = json['password'];
    login = json['login'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['password'] = this.password;
    data['login'] = this.login;
    return data;
  }
}

// zwrot JSONa przy rejestracji w celu informacji dla uzytkownika i dalszych działań programu
class RegisterResponse {
  int? status;
  String? statusText;

  RegisterResponse({this.status, this.statusText});

  RegisterResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    statusText = json['status_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['status_text'] = this.statusText;
    return data;
  }
}
