import 'package:geolocator/geolocator.dart';

Future<Position> getGeoLocationPosition() async {
  bool serviceEnabled;
  LocationPermission permission;
  // Sprawdz czy lokalizacja jest włączona
  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    // serwis lokalizacji jest nie włączony
    await Geolocator.openLocationSettings();
    return Future.error('Lokalizacja jest wyłączona.');
  }
  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // pozwolenie odmowione, nastepnym razem sprobuj jeszcze raz
      return Future.error('Lokalizacja zablokowana');
    }
  }
  if (permission == LocationPermission.deniedForever) {
    // pozwolenie jest całkowicie zablokowane
    return Future.error(
        'Lokalizacja jest całkowicie zablokowana, nie mogliśmy nic z tym zrobić.');
  }
  // Jesli sie dostalismy tutaj to pozwolenia zostaly przyznane
  return await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high);
}
